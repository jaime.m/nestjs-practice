SHELL := /bin/bash
debug := 0
recreate := 0
.DEFAULT_GOAL := up

#node managment and testing
build:
	@sudo docker-compose build
up:
	@sudo docker-compose down
ifeq (${debug},0)
	@sudo docker-compose up -d --build
	@notify-send -i wire-on 'Aplicación desplegada' 'Despliegue exitoso'
else
	@notify-send -i package_download_failed 'La aplicación se desplegará' 'presione "Ctrl+c" cuando desee deternerla' || echo 'Aplicacion desplegada'
	@sudo docker-compose up --build
	@sudo docker-compose down
endif
	@exit 0;
down:
	@sudo docker-compose down
	@exit 0;
destroy:
	@sudo docker-compose down
	@clear
	@echo "¿desea eliminar los contenedores?"
	@while [ -z "$$CONTINUE" ]; do \
        read -r -p "Presione S/s para confirmar. [s/N]: " CONTINUE; \
    done ; \
    [ $$CONTINUE = "s" ] || [ $$CONTINUE = "S" ] || (echo "Exiting."; exit 1;)
	@clear
	@echo "Eliminando los contenedores"
	# Delete all images
	@sudo docker rmi $$(sudo docker images -q) || echo ""
	@sudo docker system prune -a -f
	@sudo docker volume prune -f
	# delete storage
	@echo "¿desea eliminar la base de datos?"
	@while [ -z "$$CONTINUE" ]; do \
        read -r -p "Presione S/s para eliminar el volumen. [s/N]: " CONTINUE; \
    done ; \
    [ $$CONTINUE = "s" ] || [ $$CONTINUE = "S" ] || (echo "Exiting."; exit 1;)
	@sudo rm -rf data/db
	@exit 0;
