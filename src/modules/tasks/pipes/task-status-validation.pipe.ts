import { PipeTransform, BadRequestException } from "@nestjs/common";
import { TaskStatus } from "../task-status.enum";

export class TaskStatusValidationPipe implements PipeTransform {

  readonly allowStatuses = [
    TaskStatus.OPEN,
    TaskStatus.IN_PROGRESS,
    TaskStatus.DONE
  ];

  transform(value: any){
    console.log('value',value);
    value = value.toUpperCase();
    if(!this.isStatusValid(value)){
      throw new BadRequestException(`${value} is an invalid status`)
    }
    return value;
  }

  private isStatusValid(status: any){
    const idx = this.allowStatuses.indexOf(status);
    console.log('idx',idx);
    return idx !== -1;
  }
}
