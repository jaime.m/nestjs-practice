import { Task } from './task.entity';
import { Repository, EntityRepository } from "typeorm";
import { CreateTaskDTO } from './dto/create-task.dto';
import { TaskStatus } from './task-status.enum';
import { FilterTaskDTO } from './dto/filter-task.dto';
import { User } from '../auth/user.entity';
import { Logger, InternalServerErrorException } from '@nestjs/common';

@EntityRepository(Task)
export class TaskRepository extends Repository<Task>{

  private logger = new Logger('TaskRepository');

  async getTasks(filterTaskDTO: FilterTaskDTO, user: User): Promise<Task[]> {

    const {status,search} = filterTaskDTO;
    const query = this.createQueryBuilder('task');
    query.andWhere('task.user_id = :user',{user: user.id});
    if(status){
      query.andWhere('task.status = :status',{status});
    }
    if(search){
      query.andWhere('task.title LIKE :search OR task.description LIKE :search',{search: `%${search}%`});
    }

    try {
      const result = await query.getMany();
      return result;
    } catch(error){
      this.logger.error(`Error getting tasks for user "${user.username}" DTO: ${JSON.stringify(filterTaskDTO)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  async createTask(createTaskDTO: CreateTaskDTO, user: User): Promise<Task> {
    const {title,description} = createTaskDTO;
    const task = new Task();
    task.title = title;
    task.description = description;
    task.status = TaskStatus.OPEN;
    task.user = user;
    try {
      await task.save();
    } catch(error) {
      this.logger.error(`Error creating task for user "${user.username}" DTO: ${JSON.stringify(createTaskDTO)}`, error.stack);
      throw new InternalServerErrorException();
    }
    delete task.user;
    return task;

  }
}
