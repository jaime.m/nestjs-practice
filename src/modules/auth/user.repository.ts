import { Repository, EntityRepository } from "typeorm";
import { User } from "./user.entity";
import { AuthCredentialsDTO } from "./dto/auth-credentials.dto";
import { ConflictException, InternalServerErrorException } from "@nestjs/common";

import * as bcrypt from 'bcrypt';

@EntityRepository(User)
export class USerRepository extends Repository<User> {
  async signUp(authCredentialsDTO: AuthCredentialsDTO): Promise<void> {
    const { username, password} = authCredentialsDTO;

    const exists = this.findOne({username})

    const user = new User();
    user.username = username;
    user.salt = await bcrypt.genSalt();;
    user.password = await this.hasPassword(password, user.salt);
    try{
      await user.save();
    } catch(error){
      console.log(error);
      if(error.errno === 1062){
        throw new ConflictException('Username already exists')
      } else {
        throw new InternalServerErrorException();
      };
    }
  }

  async validateUSerPassword(authCredentialsDTO: AuthCredentialsDTO): Promise<string>{
    const {username, password} = authCredentialsDTO;
    const user  = await this.findOne({username});
    if(user && await user.validatePassword(password)){
      return user.username;
    }
    return null;
  }

  private async hasPassword(password: string, salt: string): Promise<string>{
    return bcrypt.hash(password, salt);
  }
}
