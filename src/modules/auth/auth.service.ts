import { Injectable, NotFoundException, UnauthorizedException, Logger } from '@nestjs/common';
import { USerRepository } from './user.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialsDTO } from './dto/auth-credentials.dto';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt-payload.interface';

@Injectable()
export class AuthService {

  private logger = new Logger('AuthService');

  constructor(
    @InjectRepository(USerRepository)
    private userRepository: USerRepository,
    private jwtService: JwtService,
  ){

  }

  async signUp(authCredetialsDTO: AuthCredentialsDTO): Promise<void> {
    return this.userRepository.signUp(authCredetialsDTO);
  }

  async signIn(authCredetialsDTO: AuthCredentialsDTO): Promise<{accessToken: string}> {
    const username = await this.userRepository.validateUSerPassword(authCredetialsDTO);
    if(!username){
      throw new UnauthorizedException(`Invalid credentials`);
    }
    const payload: JwtPayload = {username}
    const accessToken = await this.jwtService.sign(payload);
    this.logger.debug(`generated JWT with  payload ${JSON.stringify(payload)}`);
    return {accessToken};
  }
}
