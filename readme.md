<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

# NestJS practice

A practice for the Udemy course "Udemy - NestJS Zero to Hero - Modern TypeScript Back-end Development".

Currently runs with:

- Docker
- NodeJS
- NestJS

## Getting Started

Download the NestJS CLI and create a new project:

``` bash
sudo npm i -g @nestjs/cli
nest new nestjs-practice
```

Check the project structure:

 ```
├── dist/                   # Compiled files
├── node_modules/           # NodeJs dependencies
├── src/                    # Source files
    ├── app.module.ts       # NestJS Module application configurator
    ├── main.ts             # Endpoint
├── test/                   # Automated tests
├── .editorconfig           # EditorConfig configuration file
├── .eslintrc.js            # TypeScript lint configuration file
├── .gitignore              # Description of the files ignored in the repository
├── .prettierrc             # Prettier configuration file
├── package-lock.json       # Nodejs project file
├── package.json            # NodeJS project configuration file
├── readme.md               # Markdown project description
├── tsconfig.build.json     # TypeScript compiler build file
└── tsconfig.json           # TypeScript compiler configuration file
 ```

## Coding

Your project structure depends of  `Modules` and  `Controllers` :

### Modules

Bloc of code designed to create a scalable application, it works as a singleton and can contain other modules:

```
# you can create a module using
nest g module <modules/[module name]>
```

### Controllers

The controllers are responsible for handling the requests and the responses using the handlers

```
# you can create a controller using
nest g module <modules/[module - controller name]> --no-spec
```

### Providers

Provides to a controller or a module objects, values or functions to provide a specific service or function:

```
# you can create a controller using
nest g service <modules/[module - controller name]> --no-spec
```





## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start
# watch mode
$ npm run start:dev
# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test
# e2e tests
$ npm run test:e2e
# test coverage
$ npm run test:cov
```

## License

  Nest is [MIT licensed](LICENSE).
